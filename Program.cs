﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Drawing;
using System.Drawing.Imaging;

namespace example_cef_offscreen_tcp_client
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length < 2) 
            {
                Console.Out.WriteLine("Usage: <exe> Port Command [js]");
            }
            PrintArgs(args);
            RunCommand(args);            
        }

        static void PrintArgs(string[] args) 
        {
            Console.Out.WriteLine(String.Format("args[0] = {0}", args[0]));
            Console.Out.WriteLine(String.Format("args[1] = {0}", args[1]));
            if(args.Length > 2) 
            {
                Console.Out.WriteLine(String.Format("args[2] = {0}", args[2]));
            }
        }

        static void RunCommand(string[] args)
        {
            int port = Int32.Parse(args[0]);
            string command = args[1];
            string val = null;
            if(args.Length > 2) 
            {
                val = args[2];
            }
            
            TcpClient tcpClient = new TcpClient("127.0.0.1", port);
            NetworkStream stream = tcpClient.GetStream();

            string message = command;
            if(val != null) 
            {
                message = $"{command}:{val}";
            }

            byte[] messageBytes = Encoding.ASCII.GetBytes(message);
            stream.Write(messageBytes, 0, messageBytes.Length);

            if(command == "screenshot") 
            {
                try 
                {
                    Image screenshot = Bitmap.FromStream(stream);
                    string dateFileName = DateTime.Now.ToString("yyyyMMdd_hhmmss");
                    string fileName = $"/home/shanehaw/screenshots/{dateFileName}";
                    Console.WriteLine($"Try to save screenshot to {fileName}");
                    screenshot.Save(fileName, ImageFormat.Png);
                    Console.WriteLine($"Saved screenshot to {fileName}");
                }
                catch(Exception ex) 
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            else
            {                
                string response = ReadMessageFromStream(stream);
                Console.WriteLine($"Response = '{response}'");
            }
        }

        private static string ReadMessageFromStream(NetworkStream stream)
        {
            byte[] messageBytes = new byte[1024];
            int position = 0;
            StringBuilder messageBuilder = new StringBuilder();

            while (!stream.DataAvailable)
            {
                //Wait for data to come through
                Thread.Sleep(100);
            }

            //While there is data read it into the string builder
            while (stream.DataAvailable)
            {
                int amountRead = stream.Read(messageBytes, position, 1024);
                position += amountRead;
                char[] messageChars = Encoding.ASCII.GetString(messageBytes).ToCharArray();
                messageBuilder.Append(messageChars, 0, amountRead);
            }
            return messageBuilder.ToString();
        }
    }
}
